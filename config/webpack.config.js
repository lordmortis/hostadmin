// Example webpack configuration with asset fingerprinting in production.
'use strict';

var path = require('path');
var webpack = require('webpack');
var StatsPlugin = require('stats-webpack-plugin');

var TARGET = process.env.TARGET;
var ROOT_PATH = path.join(path.resolve(__dirname),'..');

// must match config.webpack.dev_server.port
// Port 3808 is the default.
var devServerPort = 3808;

// set TARGET=production on the environment to add asset fingerprints &
// omit hot-reloading
var production = (TARGET === 'production') || ((TARGET === 'staging'));

// Set to development environment if not in production mode
var EnvPlugin = new webpack.DefinePlugin({
  __DEV__: !production
});

// set NO_HOT_RELOAD=1 to disable hot reloading, even in development
var hotReload = !production && !process.env.NO_HOT_RELOAD;

var config = {
  entry: {
    'AccountEditor':  [ 'babel/polyfill', './webpack/AccountEditor.jsx' ],
    'DomainEditor':  [ 'babel/polyfill', './webpack/DomainEditor.jsx' ]
  },
  output: {
    // Build assets directly in to public/webpack/, let webpack know
    // that all webpacked assets start with webpack/

    // must match config.webpack.output_dir
    path: path.join(ROOT_PATH, 'public', 'webpack'),
    publicPath: '/webpack/',

    filename: production ? '[name]-[chunkhash].js' : '[name].js'
  },
  node: {
    fs: 'empty'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: hotReload ? ['react-hot-loader/webpack', 'babel'] : ['babel'],
        include: path.resolve(ROOT_PATH, 'webpack')
      },
      {
        test: /\.json?$/,
        loader: 'json-loader',
        include: path.resolve(ROOT_PATH, 'webpack')
      }
      // Expose React to the global context
      // {
      //   test: require.resolve('react'), loader: 'expose?React'
      // }
    ]
  },
  plugins: [
    // must match config.webpack.manifest_filename
    new StatsPlugin('manifest.json', {
      // We only need assetsByChunkName
      chunkModules: false,
      source: false,
      chunks: false,
      modules: false,
      assets: true
    })
  ],
  resolve: {
    extensions: [
      '',
      '.js',
      '.jsx',
      '.css',
      '.sass',
      '.png',
      '.svg',
      '.gif',
      '.jpg',
      '.jpeg'
    ],
    root: path.join(ROOT_PATH, 'webpack')
  }
};

if (production) {
  config.plugins.push(
    EnvPlugin,
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: { warnings: false },
      sourceMap: false
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify('production') }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin()
  );
} else {
  config.devServer = {
    port: devServerPort,
    headers: { 'Access-Control-Allow-Origin': '*' }
  };
  config.output.publicPath = '//localhost:' + devServerPort + '/webpack/';
  // Source maps
  config.devtool = 'inline-source-map';
}

if (hotReload) {
  // react-hot-loader configuration bits
  config.plugins = config.plugins.concat([
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    EnvPlugin
  ]);
  Object.keys(config.entry).forEach(function (entryPoint) {
    if (!Array.isArray(config.entry[entryPoint])) {
      config.entry[entryPoint] = [config.entry[entryPoint]];
    }
    config.entry[entryPoint] = [
      'webpack-dev-server/client?http://0.0.0.0:' + devServerPort,
      'webpack/hot/only-dev-server'
    ].concat(config.entry[entryPoint]);
  });
  config.devServer.hot = true;

}

module.exports = config;
