RECAPTCHA_SECRETS_FILE = Rails.root.join('config/recaptcha.yml')

Recaptcha.configure do |config|
  config.public_key  = 'junk'
  config.private_key = 'junk'
#  config.api_version = 'v2'

  if File.exist?(RECAPTCHA_SECRETS_FILE)
    recaptcha_yaml = YAML.load_file(RECAPTCHA_SECRETS_FILE)
    unless recaptcha_yaml.blank?
      config.public_key = recaptcha_yaml["public"]
      config.private_key = recaptcha_yaml["private"]
    end
  else
    print "WARNING: Could not find recaptcha config file #{RECAPTCHA_SECRETS_FILE}\n"
  end

end
