if Rails.env.development?
  require 'byebug/core'

  port = rand(32768) + 1024
  port = ENV.fetch("DEBUG_PORT", port).to_i

  # write port to file
  file = File.open("tmp/debugger_port", "w")
  file.write(port)
  file.write("\n")
  file.close()

  # start remote debugger
  print "Starting remote debugger on port #{port}\n"
  Byebug.start_server 'localhost', port
end

