# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if User.find_by_email('admin@sektorseven.net').blank?
  admin = User.new({
    email: "admin@sektorseven.net",
    password: 'password',
    password_confirmation: 'password',
    username: 'admin',
    status: :admin
  })
  admin.skip_confirmation!
  admin.save!
end

if User.find_by_email('user@sektorseven.net').blank?
  user = User.new({
    email: "user@sektorseven.net",
    password: 'password',
    password_confirmation: 'password',
    username: 'user'
  })
  user.skip_confirmation!
  user.save!
end

if User.find_by_email('disabled@sektorseven.net').blank?
  disabled = User.new({
    email: "disabled@sektorseven.net",
    password: 'password',
    password_confirmation: 'password',
    username: 'disabled'
  })
  disabled.skip_confirmation!
  disabled.save!
end
