class AddFieldsToUser < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      t.string  :username, null: false
      t.column :status, :integer, default: 0
    end
  end
end
