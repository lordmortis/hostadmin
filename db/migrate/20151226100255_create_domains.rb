class CreateDomains < ActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.string :name, null: false
      t.integer :features_store, array: true
      t.timestamps null: false
    end
  end
end
