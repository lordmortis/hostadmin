class CreateDomainManagers < ActiveRecord::Migration
  def change
    create_table :domain_managers, id: false do |t|
      t.belongs_to :domain, index: true, null: false, foreign_key: true
      t.belongs_to :user, index: true, null: false, foreign_key: true
      t.timestamps null: false
    end
  end
end
