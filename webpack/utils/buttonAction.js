import Logger from 'utils/Logger';

export default function() {
  const event = arguments[arguments.length - 2];
  event.preventDefault();
  if (typeof(arguments[0]) != 'function') {
    Logger.Error('Argument 1 isn\'t a function.');
    return;
  }

  arguments[0].apply(this, Array.from(arguments).slice(1, -1));
}
