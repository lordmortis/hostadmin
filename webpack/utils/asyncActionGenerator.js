import Logger from 'utils/Logger';

export default function asyncActionGenerator(type, ...argNames) {
  if (typeof(type) !== 'string') {
    Logger.error('action type must be defined, a string and is the first argument');
    return undefined;
  }
  if (argNames.length === 0) {
    argNames.push('value');
  }

  function createAction(...args) {
    const action = { type };
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index];
    });
    return action;
  }

  createAction.type = type;
  return createAction;
}
