import Logger from 'utils/Logger';

class Config {
  constructor() {
    this.Environment = undefined;
  }

  setEnvironment(env) {
    if (env === 'development') {
      Logger.Info('Development mode enabled');
    }
    this.Environment = env;
  }

  getEnvironment() {
    return this.Environment;
  }
}

const config = new Config();
export default config;
