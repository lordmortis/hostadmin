/*eslint-disable no-console */

export default class Logger {
  static Info(message) {
    if (__DEV__) {
      if (console && console.info) {
        console.info(message);
      }
    }
  }
  static Error(message) {
    if (__DEV__) {
      if (console && console.error) {
        console.error(message);
      }
    }
  }
  static Debug(message) {
    if (__DEV__) {
      if (console && console.log) {
        console.log(message);
      }
    }
  }
}

/*eslint-enable no-console */
