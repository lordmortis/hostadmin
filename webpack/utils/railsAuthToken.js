import Logger from 'utils/Logger';

function metaElementsWithName(name) {
  const elements = [];
  const elementsWithName = document.getElementsByName(name);
  for (let i = 0; i < elementsWithName.length; i++) {
    const element = elementsWithName.item(i);
    if (element.tagName.toLowerCase() == 'meta') {
      elements.push(element);
    }
  }

  return elements;
}

export default function () {
  const tokenElements = metaElementsWithName('csrf-token');
  const paramElements = metaElementsWithName('csrf-param');
  if (tokenElements.length > 0 && paramElements.length > 0) {
    return {param: paramElements[0].content, token: tokenElements[0].content};
  } else {
    Logger.error('could not find rails CSRF values');
  }
}
