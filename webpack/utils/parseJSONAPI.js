function getID(jsonData) {
  if (jsonData.id) {
    return jsonData.id;
  } else {
    return null;
  }
}

export default function(jsonData) {
  const returnObj = {id: getID(jsonData)};
  if (jsonData.attributes) {
    Object.keys(jsonData.attributes).forEach(function(attribute) {
      returnObj[attribute] = jsonData.attributes[attribute];
    });
  }
  return returnObj;
}
