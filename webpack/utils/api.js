import superagent from 'superagent';
import superagentJsonapify from 'superagent-jsonapify';

import railsAuthToken from 'utils/railsAuthToken';

superagentJsonapify(superagent);

const csrfAuth = railsAuthToken();

function update(object, original) {
  return new Promise(function (resolve, reject) {
    let agent = superagent;
    var data;
    if (original == undefined) {
      data = { data: object.fields() };
    } else {
      data = { data: object.diffFields(original) };
    }

    if (csrfAuth) {
      data[csrfAuth.param] = csrfAuth.token;
    }

    var path = `/api/${data.data.type}`;
    if (object.id) {
      agent = superagent.patch(`${path}/${object.id}`);
    } else {
      agent = superagent.post(`${path}`);
    }
    agent
      .set('Accept', 'application/vnd.api+json')
      .set('Content-Type', 'application/vnd.api+json')
      .send(data)
      .end(function(err, res) {
        if (err) {
          if (res.body && res.body.errors) {
            reject(res.body.errors);
          } else {
            reject(err);
          }
        } else {
          const newObject = object.clone();
          newObject.update(res.body.data);
          resolve(newObject);
        }
      });
  });
}

export default {
  update: update
};
