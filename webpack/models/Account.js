import ResourceBase from 'models/ResourceBase';

export default class Account extends ResourceBase {
  constructor(data) {
    super(data, ['email', 'status', 'username', 'new-password'], 'accounts');
    this.updating = false;
  }

  clone() {
    return new Account(this);
  }

  editable() {
    return this.status !== 'disabled';
  }

  disabled() {
    return this.status === 'disabled';
  }

  enabled() {
    return this.status !== 'disabled';
  }

  admin() {
    return this.status === 'admin';
  }

  user() {
    return this.status === 'active';
  }

}
