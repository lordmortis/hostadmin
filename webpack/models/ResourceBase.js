function setFields(data) {
  this[fieldListSym].forEach((field) => {
    if (data[field] !== null && data[field] !== undefined) {
      this[field] = data[field];
    }
  });
}

const fieldListSym = Symbol();
const resourceNameSym = Symbol();

export default class ResourceBase {
  constructor(data, fieldList, resourceName) {
    this[fieldListSym] = fieldList;
    this[resourceNameSym] = resourceName;
    if (data) {
      setFields.call(this, data);
      if (data.id) { this.id = data.id; }
    }
  }

  fields() {
    const object = {};
    object.type = this[resourceNameSym];
    object.id = this.id;
    object.attributes = {};
    this[fieldListSym].forEach((field) => {
      object.attributes[field] = this[field];
    });
    return object;
  }

  diffFields(original) {
    // todo - this needs to handle objects/arrays better...
    const diffFields = {
      attributes: {},
      id: this.id,
      type: this[resourceNameSym]
    };
    this[fieldListSym].forEach((field) => {
      if (this[field] != original[field]) {
        diffFields.attributes[field] = this[field];
      }
    });

    return diffFields;
  }

  update(newFields) {
    setFields.call(this, newFields);
  }
}
