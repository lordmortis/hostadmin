import ResourceBase from 'models/ResourceBase';

export default class Domain extends ResourceBase {
  constructor(data) {
    super(data, ['name', 'features'], 'domains');
  }

  clone() {
    const domain = new Domain(this);
    domain.features = [];
    for (var index in this.features) {
      domain.features.push(this.features[index]);
    }
    return domain;
  }

  featureString() {
    return this.features.join(' ');
  }
}
