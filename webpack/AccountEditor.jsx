import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import config from 'utils/Config';

import actions from 'actions/AccountEditor';
import reducer from 'reducers/AccountEditor';

import AccountsTable from 'components/AccountEditor/Table';
import AccountEditorForm from 'components/AccountEditor/Editor';
//import StatusBar from 'components/StatusBar';
//

class AccountEditor extends React.Component {
  render() {
    return (
      <div>
        <AccountsTable/>
        <AccountEditorForm/>
      </div>
    );
  }
}

const myCreateStore = compose(
  applyMiddleware(thunk),
  // This middleware enables the chrome extension (if it's available)
 typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
)(createStore);

const store = myCreateStore(reducer);

const appContainer = document.getElementById('account-editor');
const accountData = JSON.parse(appContainer.getAttribute('data-accounts'));
const accountID = JSON.parse(appContainer.getAttribute('data-account-id'));
store.dispatch(actions.loadAllFromJSON(accountData));
store.dispatch(actions.currentAccountID(accountID));

if (appContainer) {
  if (__DEV__) {
    config.setEnvironment('development');
  }
  ReactDOM.render(
     <Provider store={store}>
      <AccountEditor/>
    </Provider>,
    appContainer);
}

