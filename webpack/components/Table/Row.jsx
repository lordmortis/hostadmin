import React from 'react';

function getFieldForPath(object, path) {
  const parts = path.split('.');
  const field = parts.shift();
  if (parts.length == 0) {
    if (typeof(object[field]) === 'function') {
      return object[field]();
    } else {
      return object[field];
    }
  } else {
    return getFieldForPath(object[field], parts.join('.'));
  }
}

function renderCell(column) {
  let key = column.value;
  let value;
  if (typeof(column.value) === 'function') {
    key = column.key;
    value = column.value(this.props.item);
  } else {
    value = getFieldForPath(this.props.item, column.value);
  }

  return <td key={key}>{value}</td>;
}

function enabledActionCell(enabled, item) {
  if (typeof(enabled) === 'function') {
    return enabled(item);
  } else if (typeof(enabled) === 'string') {
    return getFieldForPath(item, enabled);
  } else if (enabled != null) {
    return enabled;
  }
}

function renderActionCell(action) {
  let label = 'action';
  let key = 'action_';
  if (action.label) {
    label = action.label;
    key = label;
  }

  if (this.props.item && this.props.item.id) {
    key += this.props.item.id;
  }

  if (this.props.item && typeof(action.onClick) === 'function') {
    if (enabledActionCell(action.enabled, this.props.item)) {
      return <button className={action.className} key={key} onClick={action.onClick.bind(null, this.props.item)}>{label}</button>;
    } else {
      return null;
    }
  } else {
    return <span key={key}>{label}</span>;
  }
}

function renderActionCells(actions) {
  const renderedActions = actions.map(renderActionCell.bind(this));
  return <td className='actions' key='actions'>{renderedActions}</td>;
}

export default class TableRow extends React.Component {
  render() {
    const cells = this.props.columns.map(renderCell.bind(this));
    if (this.props.actions && this.props.actions.length > 0) {
      cells.push(renderActionCells.call(this, this.props.actions));
    }

    if (this.props.item.updating === true) {
      return <tr><td>Updating...</td></tr>;
    } else {
      return <tr>{cells}</tr>;
    }


  }
}
