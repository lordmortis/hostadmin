import React from 'react';

import Row from 'components/Table/Row';

function idForItem(item, idPath) {
  if (idPath == null) {
    if (item.id !== undefined) {
      return item.id;
    }
  } else {
    if (typeof(idPath) === 'function') {
      return idPath(item);
    } else {
      return item[idPath];
    }
  }
  return 'unknown ID';
}

export default class TableRows extends React.Component {
  render() {
    const rows = this.props.items.map((item) => {
      return (
        <Row
            actions={this.props.actions}
            columns={this.props.columns}
            item={item}
            key={idForItem(item, this.props.idPath)}
        />
      );
    });

    return <tbody>{rows}</tbody>;
  }
}
