import React from 'react';

function header(item) {
  let key = item.value;
  if (typeof(key) === 'function') {
    key = item.key;
  }
  return <th key={key}>{item.header}</th>;
}

export default class TableHeader extends React.Component {
  render() {
    const headers = this.props.columns.map(header);
    if (this.props.actions && this.props.actions.length > 0) {
      headers.push(<th className='actions' key='actions'>{this.props.actionsHeader}</th>);
    }

    return <thead><tr>{headers}</tr></thead>;
  }
}
