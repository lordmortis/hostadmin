import React from 'react';

import AccountsStore from 'stores/Accounts';

function accountsUpdated(store) {
  this.setState({
    accountsLoading: store.loadAll,
    accountSaving: store.updatedAccount
  });
}

var accountsUpdatedFunc = Symbol();

export default class StatusBar extends React.Component {
  constructor(props) {
    super(props);
    this[accountsUpdatedFunc] = accountsUpdated.bind(this);
    this.state = {
      accountsLoading: false,
      accountSaving: null
    };
  }

  componentDidMount() {
    AccountsStore.listen(this[accountsUpdatedFunc]);
  }

  componentWillUnmount() {
    AccountsStore.unlisten(this[accountsUpdatedFunc]);
  }

  render() {
    let statusString = '';
    if (this.state.accountsLoading) {
      statusString += 'Loading Accounts';
    }

    if (this.state.accountSaving) {
      statusString += `Saving Account: ${this.state.accountSaving.id}`;
    }

    if (statusString.length === 0) {
      statusString += 'Ready';
    }

    return (
      <div>{statusString}</div>
    );
  }
}
