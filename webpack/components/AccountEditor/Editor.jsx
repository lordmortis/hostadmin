import React from 'react';
import { connect } from 'react-redux';

import actions from 'actions/AccountEditor';

function mapStateToProps(state) {
  return {
    account: state.currentAccount
  };
}

function mapDispatchToProps(dispatch) {
  return {
    cancelEditing: () => { dispatch(actions.editAccount(null)); },
    newAccount: () => { dispatch(actions.createAccount()); },
    saveAcount: (account, newData) => {
      dispatch(actions.updateAccount(account, newData));
    }
  };
}

function getField(field) {
  if (this.state.updatedFields[field] !== null && this.state.updatedFields[field] !== undefined) {
    return this.state.updatedFields[field];
  } else {
    return this.props.account[field];
  }
}

function updateField(field, event) {
  const updatedFields = this.state.updatedFields;
  const newValue = event.target.value;
  if (newValue === this.props.account[field]) {
    delete(updatedFields[field]);
  } else {
    updatedFields[field] = newValue;
  }
  this.setState({updatedFields: updatedFields});
}

function updateAccount(event) {
  this.props.saveAcount(this.props.account, this.state.updatedFields);
  event.preventDefault();
}

function createAccount(event) {
  this.props.newAccount();
  event.preventDefault();
}

function cancelEditing(event) {
  this.props.cancelEditing();
  event.preventDefault();
}

function resetFields(event) {
  this.setState({updatedFields: {}});
  event.preventDefault();
}

function renderStatusRadioButton(fieldValue, checkBoxConfig) {
  const checked = checkBoxConfig.state == fieldValue;
  return (
    <div key={checkBoxConfig.state}>
      <input checked={checked}
          key={checkBoxConfig.state}
          name='status'
          onChange={updateField.bind(this, 'status')}
          type='radio'
          value={checkBoxConfig.state}/>
      {checkBoxConfig.text}
    </div>
  );
}

function renderStatusRadioButtons() {
  const checkBoxes = [
    {state: 'disabled', text: 'Account Disabled'},
    {state: 'active', text: 'Account Active'},
    {state: 'admin', text: 'Administrator Account'}
  ];

  const fieldValue = getField.call(this, 'status');
  return checkBoxes.map(renderStatusRadioButton.bind(this, fieldValue));
}

export default class AccountEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      updatedFields: {}
    };
  }

  render() {
    if (this.props.account) {
      const email = getField.call(this, 'email');
      const username = getField.call(this, 'username');
      const newPassword = this.state.updatedFields['new-password'];
      const saveDisabled = Object.keys(this.state.updatedFields).length === 0;
      const resetDisabled = Object.keys(this.state.updatedFields).length === 0;

      return (
        <form>
          <button onClick={cancelEditing.bind(this)}>Cancel Editing</button>
          <div>
            <label name='email'>Email:</label>
            <input name='email' onChange={updateField.bind(this, 'email')} type='text' value={email}/>
          </div>
          <div>
            <label name='username'>Username</label>
            <input name='username' onChange={updateField.bind(this, 'username')} type='text' value={username}/>
          </div>
          <div>
            <label name='status'>State</label>
            {renderStatusRadioButtons.call(this)}
          </div>
          <div>
            <label name='new_password'>New Password</label>
            <input name='new_password' onChange={updateField.bind(this, 'new-password')} type='text' value={newPassword}/>
          </div>
          <button disabled={saveDisabled} onClick={updateAccount.bind(this)}>Save</button>
          <button disabled={resetDisabled} onClick={resetFields.bind(this)}>Reset</button>
        </form>
      );
    } else {
      return <button onClick={createAccount.bind(this)}>Create New Account</button>;
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountEditor);
