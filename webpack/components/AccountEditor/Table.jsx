import React from 'react';
import { connect } from 'react-redux';

import actions from 'actions/AccountEditor';

import Table from 'components/Table';

function mapStateToProps(state) {
  return {
    accounts: state.accounts,
    loading: state.accountsLoading
  };
}

function mapDispatchToProps(dispatch) {
  return {
    editAction: (account) => { dispatch(actions.editAccount(account)); },
    enableAction: (account) => { dispatch(actions.enableAccount(account)); },
    disableAction: (account) => { dispatch(actions.disableAccount(account)); }
  };
}

class AccountEditorTable extends React.Component {
  render() {
    const columns = [
      { header: 'Email', value: 'email', key: 'email' },
      { header: 'Account', value: 'username', key: 'account' },
      { header: 'State', value: 'status', key: 'state' }
    ];

    const actions = [
      {label: 'Edit', onClick: this.props.editAction, enabled: 'editable'},
      {label: 'Disable', onClick: this.props.disableAction, enabled: 'enabled'},
      {label: 'Enable', onClick: this.props.enableAction, enabled: 'disabled'}
    ];

    return (
      <div>
        <Table
            actions={actions}
            actionsHeader='Actions'
            columns={columns}
            items={this.props.accounts}
            loading={this.props.loading}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountEditorTable);
