import React from 'react';
import { connect } from 'react-redux';

import actions from 'actions/DomainEditor';

import Table from 'components/Table';

function mapStateToProps(state) {
  return {
    domains: state.domains
  };
}

function mapDispatchToProps(dispatch) {
  return {
    edit: (account) => { dispatch(actions.edit(account)); }
  };
}

class DomainEditorTable extends React.Component {
  render() {
    const columns = [
      { header: 'Domain', value: 'name', key: 'name' },
      { header: 'Features', value: 'featureString', key: 'features' }
    ];

    const actions = [
      {label: 'Edit', onClick: this.props.edit, enabled: true}
    ];

    return (
      <div>
        <Table
            actions={actions}
            actionsHeader='Actions'
            columns={columns}
            items={this.props.domains}
            loading={this.props.loading}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DomainEditorTable);
