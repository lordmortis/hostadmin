import React from 'react';
import { connect } from 'react-redux';

import actions from 'actions/DomainEditor';

import buttonAction from 'utils/buttonAction';

function mapStateToProps(state) {
  return {
    edited: state.editedDomain,
    original: state.editingDomain
  };
}

function mapDispatchToProps(dispatch) {
  return {
    cancel: () => { dispatch(actions.edit(null)); },
    new: () => { dispatch(actions.createDomain()); },
    reset: () => { dispatch(actions.reset()); },
    save: (edited, original) => {
      dispatch(actions.save(edited, original));
    },
    update: (fieldPath, event) => {
      dispatch(actions.updateValue(fieldPath, event.target.value));
    }
  };
}

function renderInput(fieldName, type='text') {
  const value = this.props.edited[fieldName];
  return (
    <input
        name={fieldName}
        onChange={this.props.update.bind(null, fieldName)}
        type={type}
        value={value}/>
  );
}

class AccountEditor extends React.Component {
  render() {
    if (this.props.original) {
      const save = buttonAction.bind(
        this, this.props.save, this.props.edited, this.props.original
      );

      const cancel = buttonAction.bind(
        this, this.props.cancel
      );

      const reset = buttonAction.bind(
        this, this.props.reset
      );

      return (
        <div>
          <button onClick={cancel}>Cancel Editing</button>
          <form>
            <div>
              <label name='name'>Domain Name:</label>
              {renderInput.call(this,'name')}
            </div>
            <button onClick={save}>Save</button>
            <button onClick={reset}>Reset</button>
          </form>
        </div>
      );
    } else {

      return (
        <div>
          <button
              onClick={buttonAction.bind(this, this.props.new)}>
                Create New Account
          </button>
        </div>
      );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountEditor);
