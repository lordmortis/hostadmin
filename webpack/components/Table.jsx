import React from 'react';

import Header from 'components/Table/Header';
import Rows from 'components/Table/Rows';

export default class Table extends React.Component {
  render() {
    if (this.props.loading) {
      return (
        <table>
          <tbody>
            <tr><td>Loading...</td></tr>
          </tbody>
        </table>
      );
    } else {
      return (
        <table>
          <Header
              actions={this.props.actions}
              actionsHeader={this.props.actionsHeader}
              columns={this.props.columns}
              key='header'
          />
          <Rows
              actions={this.props.actions}
              columns={this.props.columns}
              idPath={this.props.idPath}
              items={this.props.items}
              key='rows'
          />
        </table>
      );
    }
  }
}

Table.propTypes = {
  actions: React.PropTypes.array,
  actionsHeader: React.PropTypes.string,
  columns: React.PropTypes.array.isRequired,
  // required if the objects in 'items' don't have a unique 'id' field
  idPath: React.PropTypes.string,
  items: React.PropTypes.array.isRequired,
  loading: React.PropTypes.bool
};

function exampleFunc(object) {
  return object.col1 + object.col2 + object.col3;
}

Table.defaultProps = {
  // Following are example column types
  columns: [
    { header: 'Field', value: 'col1', key: 'first' },
    { header: 'Function', value: exampleFunc, key: 'fourth' },
    { header: 'ValuePath', value: 'obj.field1', key: 'fifth' }
  ],
  items: [
    {id: 1, col1: 1, col2: 2, col3: 3, junkcol: 'junk', obj: {field1: 'field'}},
    {id: 2, col1: 2, col2: 4, col3: 6, junkcol: 'bleat', obj: {field1: 'field'}},
    {id: 3, col1: 3, col2: 6, col3: 9, junkcol: 'test', obj: {field1: 'field'}}
  ],
  loading: false
};
