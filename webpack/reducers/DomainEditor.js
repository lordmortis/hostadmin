import Actions from 'actions/DomainEditor';
import Domain from 'models/Domain';

import parseJSONAPI from 'utils/parseJSONAPI';

const initialState = {
  domains: [],
  editingDomain: null,
  editedDomain: null
};

function copyState(state) {
  var newState = Object.assign({}, state);
  newState.domains = [];
  for (var index in state.domains) {
    newState.domains.push(state.domains[index].clone());
  }
  if (newState.editingDomain != null) {
    newState.editingDomain = state.editingDomain.clone();
  }
  if (newState.editedDomain != null) {
    newState.editedDomain = state.editedDomain.clone();
  }

  return newState;
}

function findDomain(id, state) {
  var domain = state.domains.find(function(item) {
    return item.id == id;
  });
  return domain;
}

export default function(state = initialState, action) {
  const newState = copyState(state);
  switch (action.type) {
    case Actions.loadAllFromJSON.type:
      newState.domains = action.value.data.map(function(accountData) {
        return new Domain(parseJSONAPI(accountData));
      });
      break;
    case Actions.edit.type:
      if (action.value != null) {
        newState.editingDomain = findDomain(action.value.id, newState);
        if (newState.editingDomain != null) {
          if (
              (newState.editedDomain == null) ||
              (newState.editingDomain.id != newState.editedDomain.id)
            ) {
            newState.editedDomain = newState.editingDomain.clone();
          }
        }
      } else {
        newState.editingDomain = null;
      }
      break;
    case Actions.createDomain.type:
      newState.editingDomain = new Domain();
      newState.editedDomain = new Domain();
      break;
    case Actions.updateValue.type:
      if (newState.editedDomain != null) {
        newState.editedDomain[action.field] = action.value;
      }
      break;
    case Actions.reset.type:
      if (newState.editingDomain != null) {
        newState.editedDomain = newState.editingDomain.clone();
      }
      break;
    case Actions.saving.type:
      var domain = findDomain(action.value.id, newState);
      if (domain != null) domain.updating = true;
      break;
    case Actions.saved.type:
      domain = findDomain(action.value.id, newState);
      if (domain != null) {
        for (var key in action.value) domain[key] = action.value[key];
      } else {
        newState.domains.push(action.value);
      }
      break;
    default:
      return state;
  }

  return newState;
}
