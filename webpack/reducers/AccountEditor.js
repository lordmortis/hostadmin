import Account from 'models/Account';
import Actions from 'actions/AccountEditor';

import parseJSONAPI from 'utils/parseJSONAPI';

const initialState = {
  account: null,
  updating: false,
  accounts: [],
  accountsLoading: false,
  currentAccount: null,
  myAccount: null
};

function copyState(state) {
  var newState = Object.assign({}, state);
  newState.account = Object.assign({}, newState.account);
  if (state.currentAccount != null) {
    newState.currentAccount = Object.assign({}, state.currentAccount);
  }
  if (state.myAccount != null) {
    newState.myAccount = Object.assign({}, newState.myAccount);
  }
  newState.accounts = [];
  for (var index in state.accounts) {
    newState.accounts.push(state.accounts[index].clone());
  }
  return newState;
}

function setMyAccount(state) {
  if (state.myAccount == null) return;
  var account;
  if (typeof(state.myAccount) === 'object') {
    account = findAccount(state.myAccount.id, state);
  } else {
    account = findAccount(state.myAccount, state);
  }
  if (account != null) {
    state.myAccount = account;
  }
}

function findAccount(accountID, state) {
  return state.accounts.find(function(item) {
    return item.id == accountID;
  });
}

export default function(state = initialState, action) {
  const newState = copyState(state);
  switch (action.type) {
    case Actions.loadAllFromJSON.type:
      newState.accounts = action.value.data.map(function(accountData) {
        return new Account(parseJSONAPI(accountData));
      });
      setMyAccount(newState);
      break;
    case Actions.currentAccountID.type:
      newState.myAccount = action.value;
      setMyAccount(newState);
      break;
    case Actions.updatingAccount.type:
      var account = findAccount(action.value.id, newState);
      if (account != null) account.updating = true;
      break;
    case Actions.updatedAccount.type:
      account = findAccount(action.value.id, newState);
      if (account != null) {
        for (var key in action.value) account[key] = action.value[key];
      } else {
        newState.accounts.push(new Account(action.value));
      }
      break;
    case Actions.editAccount.type:
      if (action.value != null) {
        newState.currentAccount = findAccount(action.value.id, newState);
      } else {
        newState.currentAccount = null;
      }
      break;
    case Actions.createAccount.type:
      newState.currentAccount = new Account();
      break;
    default:
      return state;
  }

  return newState;
}
