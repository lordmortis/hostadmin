import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import config from 'utils/Config';

import actions from 'actions/DomainEditor';
import reducer from 'reducers/DomainEditor';

import DomainsTable from 'components/DomainEditor/Table';
import Editor from 'components/DomainEditor/Editor';

class DomainEditor extends React.Component {
  render() {
    return (
      <div>
        <DomainsTable/>
        <Editor/>
      </div>
    );
  }
}

const myCreateStore = compose(
  applyMiddleware(thunk),
  // This middleware enables the chrome extension (if it's available)
 typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
)(createStore);

const store = myCreateStore(reducer);

const appContainer = document.getElementById('domain-editor');
const domainsData = JSON.parse(appContainer.getAttribute('data-domains'));
store.dispatch(actions.loadAllFromJSON(domainsData));

if (appContainer) {
  if (__DEV__) {
    config.setEnvironment('development');
  }
  ReactDOM.render(
     <Provider store={store}>
      <DomainEditor/>
    </Provider>,
    appContainer);
}
