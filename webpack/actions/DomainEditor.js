import API from 'utils/api';
import Domain from 'models/Domain';
import actionGenerator from 'utils/actionGenerator';

const prefix = 'DomainsEditor';

const saved = actionGenerator(`${prefix}Saved`);
const saving = actionGenerator(`${prefix}Saving`);
const saveError = actionGenerator(`${prefix}SaveError`);

export default {
  loadAllFromJSON: actionGenerator(`${prefix}LoadFromJSON`),
  edit: actionGenerator(`${prefix}Edit`),
  createDomain: actionGenerator(`${prefix}Create`),
  reset: actionGenerator(`${prefix}EditReset`),
  updateValue: actionGenerator(`${prefix}UpdateValue`, 'field', 'value'),
  save: function(domain, originalDomain) {
    return function(dispatch) {
      dispatch(saving(originalDomain));
      return API.update(domain, originalDomain).then(function(data) {
        dispatch(saved(data));
      }, function(error) {
        dispatch(saveError(error));
      });
    };
  },
  saveError: saveError,
  saved: saved,
  saving: saving
};
