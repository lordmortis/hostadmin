import Account from 'models/Account';
import API from 'utils/api';
import actionGenerator from 'utils/actionGenerator';

const prefix = 'AccountEditor';

const updatedAccount = actionGenerator(`${prefix}AccountUpdated`);
const updatingAccount = actionGenerator(`${prefix}AccountUpdating`);
const updatingAccountError = actionGenerator(`${prefix}AccountUpdatingError`);

function updateAccount(dispatch, account, newData) {
  const newAccount = new Account(newData);
  newAccount.id = account.id;
  dispatch(updatingAccount(account));
  return API.update(newAccount).then(function(data) {
    dispatch(updatedAccount(data));
  }, function(error) {
    dispatch(updatingAccountError(error));
  });
}

export default {
  // Sync actions
  loadAllFromJSON: actionGenerator(`${prefix}LoadFromJSON`),
  currentAccountID: actionGenerator(`${prefix}CurrentAccountID`),
  editAccount: actionGenerator(`${prefix}EditAccount`),
  savingAccount: actionGenerator(`${prefix}SavingAccount`),
  savedAccount: actionGenerator(`${prefix}SavedAccount`),
  createAccount: actionGenerator(`${prefix}CreateAccount`),
  updatingAccount: updatingAccount,
  updatedAccount: updatedAccount,

  // Async actions
  enableAccount: function(account) {
    return function(dispatch) {
      return updateAccount(dispatch, account, {status: 'active'});
    };
  },
  disableAccount: function(account) {
    return function(dispatch) {
      return updateAccount(dispatch, account, {status: 'disabled'});
    };
  },
  updateAccount: function(account, newData) {
    return function(dispatch) {
      return updateAccount(dispatch, account, newData);
    };
  }
};
