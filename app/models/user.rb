class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :lockable, :recoverable, :rememberable, :trackable, :validatable

  enum status: [:active, :disabled, :admin]

  has_many :domain_managers, dependent: :destroy
  has_many :domains, through: :domain_managers
end
