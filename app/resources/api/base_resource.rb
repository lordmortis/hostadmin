class Api::BaseResource < JSONAPI::Resource
  [:create, :update, :remove].each do |action|
    set_callback action, :before, :authorize
  end

  # Authorize the model for the permission required by the controller
  # action. Also, mark the controller as having been policy authorized.
  def authorize
    permission = context[:operation] + "?"
    policy = Pundit.policy!(context.fetch(:current_user), @model)
    unless policy.public_send(permission)
      raise Pundit::NotAuthorizedError.new(query: context[:operation], record: @model, policy: policy)
    end
  end

  class << self
  end
end
