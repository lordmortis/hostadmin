class Api::AccountResource < Api::BaseResource
  model_name 'User'
  attributes :email, :username, :status, :new_password

  def new_password=(value)
    @model.password = value
    @model.password_confirmation = value
  end

  def fetchable_fields
    super - [:new_password]
  end

end
