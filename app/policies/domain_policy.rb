class DomainPolicy < ApplicationPolicy

  def index?
    user_is_admin? || user_manages_domain?
  end

  def create?
    user_is_admin?
  end

  def update?
    user_is_admin? || user_manages_domain?(@record)
  end

  def user_manages_domain?(domain = nil)
    if domain.blank?
      user_is_active? && @user.domains.count > 0
    else
      user_is_active? && @user.domains.exists?(domain)
    end
  end

  class Scope < Scope
    def resolve
      if user_is_admin?
        scope
      else
        scope.where(domain: @user.domains)
      end
    end
  end
end
