class Api::BaseController < ActionController::Base
  include JSONAPI::ActsAsResourceController

  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :reject_forbidden_request

  def context
    {current_user: current_user, operation: action_name}
  end

  def handle_exceptions(e)
    if e.class.ancestors.include? Pundit::NotAuthorizedError
      render json: {error: 'Forbidden'}, :status => 403
    else
      super(e)
    end
  end
end
