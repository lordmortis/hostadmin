class IndexController < ApplicationController
  def index
  end

  def accounts
    authorize :user, :index?
    resources = User.all.collect { |user| Api::AccountResource.new(user, context) }
    @accounts = JSONAPI::ResourceSerializer.new(Api::AccountResource).serialize_to_hash(resources)
  end

  def domains
    authorize :domain, :index?
    resources = policy_scope(Domain).all.collect { |domain|  Api::DomainResource.new(domain, context) }
    @domains = JSONAPI::ResourceSerializer.new(Api::DomainResource).serialize_to_hash(resources)
  end

private
  def context
    {current_user: current_user, operation: 'index'}
  end
end
